# Slink Documentation

[[_TOC_]]

## Useful Links

  - [DTH-P1](https://cms-daq-dth-p1.web.cern.ch/)
  - [SlinkRocket](https://cms-daq-dth-p2.web.cern.ch/slinkrocket): contains a compatibility table for Slink firmware versions and DTH firmware versions.
  - [SlinkRocket Users Manual](https://gitlab.cern.ch/cms_daq_dth/slinkrocket_ips/-/raw/master/doc/SlinkRocketIPCores.pdf?inline%253Dfalse)

## Interface Overview

### Data Sources

1. SLink input records from payload
2. SLink input records from dummy packet generator
3. Pre-formatted SLink Rocket packets from IPBus played through RAM
4. Formatted packet capture to IPBus before transmission to SLink Rocket cores

![slink-data-sources](images/slink-data-sources.png)

## TIF System Setup

Full documentation on access to boards can be found in the [TWiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/Ph2TrackerTIF).

 - Serenity #13: `cmx@cms-trk-serenity-2`
 - DTH: `DTH@dth-p1-v2-11`
 - PC: `tifdaq@aethon`

### Serenity Configuration

 - Serenity: transceivers: `X0Y8` & `X0Y9`
 - SLinkRocket version: `v03.12`
 - DTH firmware: `dth_p1v2_daq_25g156gty_002_002_002.bin`
 - EMP firmware and software: `master` as of May 6th 2024

## Firmware Configuration

SLink is configured in two places within the EMP framework:

1. `components/links/slink/firmware/cfg/slink_config.dep` 
This is used to select the SR core configuration for the build.
```
#
# specify SR core parameters here.  These are used both to
# select the needed flavor of SR core to instantiate, as
# well as generics for emp_slink_interface to configure
# the assoicated QPLL
#

@slink_xcvr_type 	= "gty" # gty or gth
@slink_line_rate 	= "25g" # 25g or 16g
@slink_refclk_freq  = "322.26" # 156.25 or 322.26
``` 

2. `projects/examples/serenity/dc_ku15p/firmware/hdl/sm1/emp_project_decl.vhd` 
This is used to select the quad for the interface to use, and follows the same numbering convention as the backend links.  Comment out your slink regions in the REGION_CONF used by the backend, eg:
```
constant REGION_CONF : region_conf_array_t := (
		0  	=> (gth16, buf, no_fmt, buf, gth16),
		1  	=> (gth16, buf, no_fmt, buf, gth16),
		2  	=> (gth16, buf, no_fmt, buf, gth16),
		3  	=> (gth16, buf, no_fmt, buf, gth16),
		4  	=> (gth16, buf, no_fmt, buf, gth16),
		5  	=> (gth16, buf, no_fmt, buf, gth16),
		6  	=> (gth16, buf, no_fmt, buf, gth16),
		7  	=> (gth16, buf, no_fmt, buf, gth16),
		8  	=> (gth16, buf, no_fmt, buf, gth16),
		9  	=> (gth16, buf, no_fmt, buf, gth16),
		-- Cross-chip
		10 	=> (gty25, buf, no_fmt, buf, gty25),
		11 	=> (gty25, buf, no_fmt, buf, gty25),
		12 	=> (gty25, buf, no_fmt, buf, gty25),
		--13 	=> (no_mgt, buf, no_fmt, buf, no_mgt),
		--14 	=> (no_mgt, buf, no_fmt, buf, no_mgt),
		--15 	=> (no_mgt, buf, no_fmt, buf, no_mgt), -- for Slink
		--16 	=> (gty25, buf, no_fmt, buf, gty25),
		17 	=> (gty25, buf, no_fmt, buf, gty25),
		others => kDummyRegion
		);

  	-- Specify the slink quad using the corresponding region conf ID
  	-- Specify slink channels to enable using the channel mask
  	constant SLINK_CONF : slink_conf_array_t := (
		0  	=> (15, x"3"), -- region 15, channel 0-Y8, channel 1-Y9
		others => kNoSlink
		);  
```

## Testing

### Setup

#### Serenity #13: site x1

First export the path to your firmware package and ensure it contains a `connections.xml` file.

```bash
export PACKAGE=/home/cmx/dmonk/test-serenity13-min_phlegon_240506_1508
```

Configure the FPGA and fireflys (note that the name of the bitfile may be different)

```bash
serenitybutler power cycle x1
serenitybutler fireflys configure
serenitybutler clocks configure --async-config $HOME/khahn/Si5345-RevB-ASYN322F-322.265625MHz-XTAL-Registers-v1.txt x1
serenitybutler artix reset internal

serenitybutler program x1 $PACKAGE/test-serenity13-min.bit
```

#### Aethon PC

```bash
docker run --rm -it --network=host gitlab-registry.cern.ch/dmonk/slink-documentation/dth-recv dth_recv/dth_recv --hwcrc
```

![dth_recv](images/dth_recv.png)

#### DTH

```bash
[DTH@localhost ~]$ DTH_Control.py
```

![dth-daq-menu](images/dth-daq-menu.png)

Select the following options within the menu:

- 3 - DTH setup FED source
- 1 - Setup the system and DAQon
- 2 - DAQ on

### Checking Link Status on Serenity

```bash
empbutler -c $PACKAGE/connections.xml do x1 slink status -v
```

![](images/slink-status.png)

If the links are not up, check the notes on connectivity debugging below.

### Testing with Inputs

For all input cases, the Slink IDs need to be set once after programming. One can choose different IDs than the ones below:

```bash
empbutler -c $PACKAGE/connections.xml do x1 slink quad 0 channel 0 setSourceID  0xaaaaaaaa
empbutler -c $PACKAGE/connections.xml do x1 slink quad 0 channel 1 setSourceID  0xdddddddd
```

#### Input from the Built-in Packet Generator

Option (2) of the interface data sources.

```bash
empbutler -c $PACKAGE/connections.xml do x1 slink quad 0 generator configure --length 255 --decrement  --pause 0x3f --physics 0xee --l1a 0xbeef
empbutler -c $PACKAGE/connections.xml do x1 slink quad 0 generator enable
empbutler -c $PACKAGE/connections.xml do x1 slink quad 0 configure --source  generator
```

The output on `aethon` should looks as follows:

![](images/packet-generator.png)

#### Input from the Bypass RAM

Option (3) in the interface data sources.

```bash
empbutler -c $PACKAGE/connections.xml do x1 slink quad 0 bypass loadRAM /home/cmx/khahn/emp-toolbox/examples/slink/example_packet.dat
empbutler -c $PACKAGE/connections.xml do x1 slink quad 0 configure --source bypass
```

The output on `aethon` should looks as follows:

![](images/bypass-ram.png)

To view the packet loaded into the RAM, switch to generator or payload and then read out the RAM

```bash
empbutler -c $PACKAGE/connections.xml do x1 slink quad 0 configure --source generator
empbutler -c $PACKAGE/connections.xml do x1 slink quad 0 bypass readRAM  --reverse
```

![](images/read-ram-data.png)

#### Input from the SLink Generator in the Payload

Option (1) in the interface data sources. Set the environment before using:

```bash
export EMP_SLINK_ENABLE_PAYLOAD_COMMANDS=1

empbutler -c $PACKAGE/connections.xml do x1 slink payload_generator configure --length 0x1f --decrement  --pause 0x1f --physics 0xef --l1a 0xcafe
empbutler -c $PACKAGE/connections.xml do x1 slink payload_generator enable 1
empbutler -c $PACKAGE/connections.xml do x1 slink quad 0 configure --source payload
```

The output on `aethon` should looks as follows:

![](images/payload-generator.png)

#### Toggling inputs

With the 3 input modes setup, one can toggle between them by changing the SLink source:

```bash
empbutler -c $PACKAGE/connections.xml do x1 slink quad 0 configure --source  generator
empbutler -c $PACKAGE/connections.xml do x1 slink quad 0 configure --source bypass
empbutler -c $PACKAGE/connections.xml do x1 slink quad 0 configure --source payload
```

With the settings used earlier, the rate at the PC should be : 
 - Built-in generator (`--source generator`) : ~16.2 Gbps
 - Bypass RAM (`--souce bypass`) : ~23 Gbps
 - Payload generator (`--source payload`) : ~9.4 Gbps

#### Capturing Formatted Packets Sent to the SLink Rocket Cores

Option (4) in the interface data sources. This applies when the source mode is the internal or payload generators.

```bash
empbutler -c $PACKAGE/connections.xml do x1 slink quad 0 bypass setCaptureChannel 0
empbutler -c $PACKAGE/connections.xml do x1 slink quad 0 bypass capture
empbutler -c $PACKAGE/connections.xml do x1 slink quad 0 bypass readRAM
```

![](images/read-ram.png)

## Debugging

### Connectivity

#### Link Status on the  DTH

![](images/dth-link-status-menu.png)

Select the following options:

 - 7 - DTH_SR_debug
 - 4 - Serdes Status

The links should be locked/up for both Fireflys.

![](images/dth-link-status.png)

#### Optical Status

Check Rx power on Serenity, should see ~1 mW on 2 channels of J1:13.

```bash
serenitybutler fireflys status
```

![](images/fireflys-status.png)

Check Rx power on DTH:

```bash
[DTH@localhost ~]$ cd /home/DTH/khahn/dth_ibert
[DTH@localhost ~]$ python DTH_Ibert.py
```

![](images/dth-rx-power-menu.png)

Select the following option:
 - 6 - Read Rx power

Should see ~1 mW on two Fireflys:

![](images/dth-rx-power.png)

### Dumping to File

Specify the `-f <filename>` option when running `dth_recv` on the PC.  Beware that the files quickly grow large.

![](images/save-to-disk.png)

The outputs are binary files (in network order) that can be viewed with `hexdump`:

![](images/hexdump.png)


## Glossary

 - SR: SLink Rocket
